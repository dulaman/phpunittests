<?php
namespace App;

/**
 * @author adam
 */
class Franc extends Money
{
    /**
     * @param integer $amount
     */
    public function __construct($amount)
    {
        $this->amount = $amount;
    }
    
    /**
     * @param integer $multiplier
     * @return Money
     */
    public function times($multiplier)
    {
        return new Franc($this->amount * $multiplier);
    }
    
}
