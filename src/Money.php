<?php
namespace App;

/**
 * @author adam
 */
class Money {
    
    /**
     * @var integer
     */
    protected $amount;
    
    /**
     * @param \App\Money $object
     * @return bool
     */
    public function equals(Money $object)
    {
        return $this->amount === $object->amount;
    }

    /**
     * @param int $amount
     * @return \App\Money
     */
    public static function dollar($amount)
    {
       return new Dollar($amount); 
    }

    /**
     * @param int $amount
     * @return \App\Money
     */
    public static function franc($amount)
    {
       return new Franc($amount); 
    }
}
