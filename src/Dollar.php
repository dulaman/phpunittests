<?php
namespace App;

/**
 * @author adam
 */
class Dollar extends Money
{    
    /**
     * @param integer $amount
     */
    public function __construct($amount)
    {
        $this->amount = $amount;
    }
    
    /**
     * @param integer $multiplier
     * @return Money
     */
    public function times($multiplier)
    {
        return new Dollar($this->amount * $multiplier);
    }
    
}
