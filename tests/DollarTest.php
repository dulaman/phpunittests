<?php
namespace Test;

use App\Dollar;
use App\Franc;
use App\Money;

/**
 * @author adam
 */
class DollarTest extends \PHPUnit_Framework_TestCase
{
    
    public function testMultiplication()
    {
        $five = Money::dollar(5);
        $this->assertEquals(Money::dollar(10), $five->times(2));
        $this->assertEquals(Money::dollar(15), $five->times(3));
    }
    
    public function testEquals()
    {
        $this->assertTrue(Money::dollar(5)->equals(Money::dollar(5)));
        $this->assertFalse(Money::dollar(5)->equals(new Dollar(6)));
        $this->assertTrue(Money::franc(5)->equals(Money::franc(5)));
        $this->assertFalse(Money::franc(5)->equals(Money::franc(6)));
        $this->assertTrue(Money::franc(5)->equals(Money::dollar(5)));
    }
    
}
